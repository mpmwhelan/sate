/*
 * The MIT License
 *
 * Copyright 2017 Michael Whelan, E-insights Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package sate;
import java.util.ArrayList ;
import java.io.File ;
import java.io.FileReader ;
import java.io.BufferedReader ;
import java.util.Collections ;
/**
 *
 * @author whelan
 * Reads .cnf files and returns Expression objects.
 */
public abstract class NormalForm {
    public final static int DNF=1 , CNF=2 ;
    private final static int maxLiterals = 256*126 ;  // so +/- and 0 in a byte :: now move to short


    public abstract int satisfies( boolean[] v) ; // < 0 no , >=0 yes and max element index checke was the rv
    public abstract NormalForm complement();

    
    public int literalIndex( int b) {  // a byte arg will be sign extended to short
        if(b==0) return -1 ; // so it is recognizably not an index
        if( b < 0) b = -b ;
        return b-1 ;
    }

    
    public static Expression read(File file) throws Exception {    
    BufferedReader br = new BufferedReader(new FileReader(file));
    NormalForm mNF = null ;
    String line;
    int nliterals ;
    int nclauses  = -1 ;
    ArrayList<TLit> tlit = new ArrayList<>() ;
    ArrayList<short[]> tcls = new ArrayList<>() ;
    String type = null ;
    int maxL = 0 ;
    while ((line = br.readLine()) != null) {
        line = line.trim().toLowerCase() ;
        if(! (line.startsWith("c") || line.startsWith("%"))   ) {
            if( line.startsWith("p")) {
                if(mNF != null) throw new Exception("Parse error reading - already have base description!!") ;
                
                String[] pts = line.split("\\s+");
                if(pts.length != 4) throw new Exception("Parse error reading - bad field count in- " + line) ;
                type = pts[1] ;
                nliterals = Integer.parseInt(pts[2]);
                if( nliterals > maxLiterals) throw new Exception("Max literals exceeded: " + nliterals + " vs " + maxLiterals );
                
                nclauses  = Integer.parseInt(pts[3]);
                
            }
            else {  // not a c and not a p -- must be a clause
                if(type == null) throw new Exception("Parse error reading - have clause? before spec" + line) ;
                if(line.length()>0) {
                String[] pts = line.split("\\s+");
                if(! ( pts.length == 1 && pts[0].equalsIgnoreCase("0") ))  {  // line is just 0 .. ignore
                short[] clause = new short[pts.length- 1] ;  // always ends in 0 which we can ignore
                
                for(int k=0;k<pts.length-1;k++)
                    tlit.add( new TLit(Integer.parseInt(pts[k])));
                Collections.sort(tlit);
                int k = 0;
                while(! tlit.isEmpty()) {
                  clause[k++] = (short) ( tlit.remove(0).ix);
                }
                tcls.add( clause );
                int tempL = clause[clause.length-1] ;
                if(tempL < 0) tempL = - tempL ;
                if(tempL > maxL) maxL = tempL ;
                if(! pts[pts.length-1].equals("0")) System.out.println("Bad line>" +line + "<  ending: " + pts[pts.length-1]) ;
                nclauses-- ;
                }
            }
            }
        }
    }

    Expression exp = new Expression(tcls,maxL);
    
    return exp ;
    }

    
    public static boolean[] readCertificate(File f) throws Exception {
    BufferedReader br = new BufferedReader(new FileReader(f));
    String line;
    while ((line = br.readLine()) != null) {
        line = line.trim() ;
        if(line.length() >0) {
            if(line.startsWith("SAT")) continue ;
            String[] pts = line.split("\\s+");
            boolean[] cert = new boolean[ pts.length -1] ;
            for(String s: pts) {
                int lv = Integer.parseInt(s);
                if(lv != 0) {
                    boolean val = true ;
                    if(lv<0) { lv = -lv ; val = ! val ; }
                    cert[lv-1] = val ;
                }
            }
            return cert ;
        }
      }
    throw new Exception("Cert not found...") ;
    }
    

}
