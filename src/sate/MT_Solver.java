/*
 * The MIT License
 *
 * Copyright 2017 Michael Whelan, E-insights Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package sate;

import java.util.PriorityQueue ;
import java.util.concurrent.ArrayBlockingQueue ;
import java.util.concurrent.TimeUnit ;
/**
 *
 * @author whelan
 */
public class MT_Solver {
    final ArrayBlockingQueue<SubProblem> wakeQ ;
    final ArrayBlockingQueue<Long> availQ ;
    final ArrayBlockingQueue<SatResult> resultQ ;
    private final PriorityQueue<SubProblem> subProbs ;
    private Thread[] workers ;
    private SolverWorker[] solvers ;
    private int nWaiting ;
    private Expression satX ;
    private long nSubsHandled = 0 ;
    private int smallestUnbound , origUnbound ;
    private final int maxSecondsToWork ;

    public MT_Solver( int nt , int maxSeconds  ) {
        subProbs = new PriorityQueue() ; 
        workers = new Thread[nt];
        solvers = new SolverWorker[nt] ;
        wakeQ = new ArrayBlockingQueue<>(nt) ;
        availQ = new ArrayBlockingQueue<>(1);
        resultQ = new ArrayBlockingQueue<>(1);
        
        for(int i=0;i<workers.length;i++) {
            solvers[i] = new SolverWorker(i,this);
            workers[i] = new Thread( solvers[i] ) ;
            workers[i].start() ;
        }
        nWaiting = nt ;
        
        try { availQ.put( new Long(System.currentTimeMillis() )); }
        catch(Exception x) {
            Message.traceMessage(1,"Interruption");
        }
        maxSecondsToWork = maxSeconds ;
    }

    private int pnum=0 ;
    public SatAssignment sat(Expression bx) {
        int son = bx.bestSplitOn() ;
        if(debug) System.out.print("Check for avail: nWaiting " + nWaiting);
        try { availQ.take() ; }
          catch(Exception x) {
            Message.traceMessage(1,"Interruption");
           }
        if(debug) System.out.println(" ... OK pn=" + ( ++pnum) );

        satX = null ;
        subProbs.clear() ;
        nSubsHandled = 0 ;
        origUnbound =
        smallestUnbound = bx.nUnBound;
        
        availQ.clear();
        resultQ.clear() ;
        
        subProbs.add( new SubProblem(bx,son) );
       if(debug)  System.out.println("Starting up: " + subProbs.size());
                        
        while( nWaiting > 0 && (! subProbs.isEmpty())) { 
            wakeQ.add( getNextSubP(null) ) ;
            nWaiting-- ;
            }
        // now we have to wait on result
      if(debug)   System.out.println("Wait on result: " + subProbs.size());
        SatResult res = null ;
        
        try { res = resultQ.poll(maxSecondsToWork,TimeUnit.SECONDS) ; /* was take() ;*/ 
        if(res == null) Message.exitMessage("INDETERMINATE Time limit " + maxSecondsToWork + " exceeded");
        } catch(Exception ex) {Message.exitMessage("ERROR MT_Solver.sat Interruption: " + ex);}
 
        return res.satX ;
    }
    private  final static boolean debug=false ;
    private final boolean verb = false ;

    public synchronized SubProblem spDone( SubProblem sp) {
        nSubsHandled++;
        
        if( sp != null && sp.x != null && sp.x.nUnBound < smallestUnbound)
            smallestUnbound = sp.x.nUnBound ;
        if(verb) {
            boolean note = false ;
        if(nSubsHandled < 1000000) {
            if( nSubsHandled % 100000 == 0) note=true ;
            }
        else
            if( nSubsHandled % 500000 == 0)  note = true ;
    
        if(note) {
            System.out.println("Handled: " + nSubsHandled + " unBnd " + origUnbound + "/" + smallestUnbound + " spq=" + (subProbs.size()+1) );
        }
        }
        if(debug) {
            if(sp.x != null) System.out.println("spdone with " + sp.x.nLiveClauses + " son=" + sp.splitOn );
            else System.out.println("spdone with no follow on nW- = " + nWaiting );
        }
        
        if( satX != null) { 
            nWaiting++ ;
        if(nWaiting == workers.length) {
                
                if(satX == null)
                    try { resultQ.put( new SatResult() ); }
                catch(Exception ex) {
                    Message.traceMessage(1,"Interruption");
                    }
            
                try { availQ.put( new Long(System.currentTimeMillis() )); }
                catch(Exception ex) {
                    Message.traceMessage(1,"Interruption");
                    }
        }
        return null ;
        }
        
        if(sp.x != null) {
            subProbs.add(sp);
            sp = null ; // cant re-use it
            }
        if( subProbs.isEmpty()) { 
            nWaiting++;
            if(nWaiting == workers.length) {
                try { resultQ.put( new SatResult() ) ; }
                catch(Exception ex) {
                    Message.traceMessage(1,"Interruption");
                    }
                                try { availQ.put( new Long(System.currentTimeMillis() )); }
                catch(Exception ex) {
                    Message.traceMessage(1,"Interruption");
                    }
                
               if(debug)  System.out.println("NoSat !!!" + (satX==null?"NULL satX":"Have satX") + " nWaiting=" + nWaiting);
            }
            return null ; 
        }
        
        SubProblem forMe = getNextSubP(sp)  ;
        while( nWaiting > 0 &&  ! subProbs.isEmpty()) { 
            wakeQ.add( getNextSubP(null) ) ;
            nWaiting-- ;
        }
        if(debug) System.out.println("Giving back expression: " + forMe.x.nLiveClauses + " spliton=" + forMe.splitOn +  " subProbs.size=" + subProbs.size() );
    return forMe ;
    }
    
    private SubProblem getNextSubP(SubProblem nsp) {
        SubProblem spx = subProbs.remove() ;
        
        if( spx.needToDup) {
            Expression nx = spx.x.dup() ;
            nx.coverage = spx.x.altCoverage ;
            if(nsp == null) nsp = new SubProblem(nx,- spx.splitOn,false);
            else { nsp.x=nx ; nsp.splitOn = - spx.splitOn ; nsp.needToDup = false ; }
            subProbs.add( nsp );
        }
        return spx ;
    }
    
    public synchronized void haveSat( Expression x) {
        if(satX == null) {
            if(debug) System.out.println("Have SAT: " + nWaiting);
        subProbs.clear() ;
        satX = x ;
        try { resultQ.put( new SatResult(x) ) ; }
                catch(Exception ex) {
                    Message.traceMessage(1,"Interruption");
                    }
        }
        nWaiting++ ;
                if(nWaiting == workers.length)
                try { availQ.put( new Long(System.currentTimeMillis() )); }
                catch(Exception ex) {
                    Message.traceMessage(1,"Interruption");
                    }

        if(debug) System.out.println("Have SAT: " + satX.getAssignment());
    }
    
synchronized void shutDown() {
    if(debug) System.out.println("Shutdown..");
       try { 
           for(int i=0;i<workers.length;i++)
             wakeQ.put( new SubProblem(null,0) ); 
       } catch(Exception x) {Message.traceMessage(1,"Interrupted shutdown");} 
}    
}
