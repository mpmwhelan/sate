/*
 * The MIT License
 *
 * Copyright 2017 Michael Whelan, E-insights Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package sate;

/**
 *
 * @author whelan
 * Storing subProblems in the priority queue.
 * We store before splitting to reduce memory usage, only performing the split
 * and duplication when a subProblem is being worked on.
 */
public class SubProblem implements Comparable<SubProblem> {
    Expression x ;
    int splitOn ;  // the literal to be used to split the Expression on (into 2 subProblems)
    boolean needToDup ;  // have we already duplicated and handled the other side of the split yet ?
    public SubProblem(Expression ex , int so ) {
        x = ex ;
        splitOn = so ;
        needToDup = true ;
    }
    public SubProblem(Expression ex , int so , boolean dup ) {
        x = ex ;
        splitOn = so ;
        needToDup = dup ;
    }
    public int compareTo(SubProblem sp) {
        return x.coverage - sp.x.coverage ;
    }
}
