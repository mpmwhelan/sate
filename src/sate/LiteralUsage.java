/*
 * The MIT License
 *
 * Copyright 2017 Michael Whelan, E-insights Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package sate;

import java.util.Arrays;
import java.util.ArrayList ;
/**
 *
 * @author whelan
 * Describes how a specific literal is used
 * -inClauses identifies the clauses with negation indicating complemented use.
 * NTotal could be eliminated by re-doing the initial creation process and replaced with inClauses.length
 * nComplemented (or equivalent) could be eliminated by a method.
 * May be worth exploring to reduce memory usage.
 */
public class LiteralUsage {

public static LiteralUsage noUsage = new LiteralUsage(0) ;  // so only one 'no usage' object is ever used
    
    
short[] inClauses ;
int nTotal , nComplemented ;
public LiteralUsage(int z){
nTotal = nComplemented = 0 ;
if(z>0) inClauses = new short[z]  ;
else inClauses = null ;
}

public LiteralUsage(){
nTotal = nComplemented = 0 ;
inClauses = new short[16]  ;
}

public LiteralUsage( LiteralUsage lx){
nTotal = lx.nTotal ;
nComplemented = lx.nComplemented ;
inClauses = lx.inClauses ;
}

public LiteralUsage negate() {
    short[] nic = new short[inClauses.length];
    for(int i=0;i<nic.length;i++)
        nic[i] = (short) - inClauses[i] ;
    return  new LiteralUsage( nic , nTotal ,  nTotal - nComplemented );
}
public int nTrue() {
    return nTotal-nComplemented;
}
public void add( int ci ,boolean comp) {
    if( nTotal >= inClauses.length ) {
        short[] nic = new short[inClauses.length+4];
        System.arraycopy(inClauses,0,nic,0,inClauses.length);
        inClauses = nic ;
    }
    ci++ ; // 1+
    if(comp) ci = -ci ;
    inClauses[nTotal++] = (short) ci ;
    if(comp) nComplemented ++ ;
    
}
private LiteralUsage(short[] x , int nT , int nC) {
    nTotal = nT ;
    nComplemented = nC ;
    inClauses = x ;
}
public LiteralUsage dropFromClause( int cn , boolean isComp ) { // eliminate this literal from clause cn
    if(nTotal>1) {
    short[] nic = new short[nTotal-1] ;
    cn++;
  //  if(isComp) cn = -cn ;
    int w=0;
    for(int i=0;i<nTotal;i++)
        if( inClauses[i] != cn  && inClauses[i] != -cn)
            nic[w++] = inClauses[i];
    return new LiteralUsage( nic , nTotal-1 , isComp ? (nComplemented-1): nComplemented );
    }
    else { // so this can ge garbage collected
        return noUsage ;
    }

}
public LiteralUsage expand(int nC , ArrayList<Integer> ncls ) {
    LiteralUsage lx = new LiteralUsage(nTotal+ncls.size()) ;
    lx.nTotal = nTotal + ncls.size();
    lx.nComplemented = nComplemented + nC ;
    if(nTotal>0) System.arraycopy(inClauses,0,lx.inClauses,0,nTotal);
    int pos=nTotal;
    while(! ncls.isEmpty()) {
        lx.inClauses[pos++] = ncls.remove(0).shortValue() ;
    }
    
    return lx ;
}

public String toString() {
    
    return "" + (nTotal-nComplemented) + "/" + nComplemented + " " + Arrays.toString(inClauses) ;
}
}
