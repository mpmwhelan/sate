/*
 * The MIT License
 *
 * Copyright 2017 Michael Whelan, E-insights Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package sate;

import java.util.Arrays ;
import java.util.ArrayList ;

/**
 *
 * @author whelan
 * Exression is stored in a redundant form as
 * 1. a list of clauses, each being an array of shorts where each short specifies a literal in signed (complemented or not) form
 * 2. for each literal a list of the clauses within which it is sued, again in signed form to indicate complemented or not.
 * While redundant, this allows for fast simplifications.
 */
public class Expression implements Comparable<Expression> {
    
    final Clause[] clauses ;
    int nLiveClauses ;
    private final boolean debug=false ;
    
    public Expression(ArrayList<short[]> tcls , int nlits) { // the clauses and thenumber of literals
        newPairs = 0 ;
        clauses = new Clause[tcls.size()] ;
        nLiveClauses = clauses.length ;
        asg = new byte[nlits] ;
        for(int i=0;i<asg.length;i++)
            asg[i] = UNBOUND ;
        nUnBound = nlits ;
        litUsage = new LiteralUsage[nlits] ;
        
        for(int i=clauses.length-1;i>=0;i--) {
            short[] lits =tcls.get(i);
            if(lits.length == 2) newPairs++;
            clauses[i] = new Clause( lits ) ;
            for(int j=0;j<lits.length;j++) {
                int lit = lits[j];
                boolean comp = false ;
                if(lit < 0) {
                    lit = -lit ;
                    comp = true ;
                }
                addLitUsage(lit, comp , i);
            }
        }   
    }
   
    // to create a copy of an Expression
    private Expression(Clause[] cls  , int nlc, byte[] basg , int bnubnd , ArrayList<iPair> bcons , LiteralUsage[] blu ) {
        clauses = new Clause[cls.length] ;
        System.arraycopy(cls, 0 , clauses , 0, clauses.length);
        nLiveClauses = nlc ;
        asg = new byte[basg.length];
        System.arraycopy(basg, 0, asg, 0, basg.length);
        nUnBound = bnubnd ;
        if(bcons != null) {
            constraints = new ArrayList<>() ;
            constraints.addAll(bcons);
        }
        litUsage = new LiteralUsage[blu.length] ;
        System.arraycopy(blu,0,litUsage,0,litUsage.length);
    }
    
    // need to dup - can do shallow copies
    Expression dup() {
        Expression d = new Expression(clauses,nLiveClauses,asg,nUnBound,constraints,litUsage) ;
        d.newPairs = newPairs ;
        return d ;
    }
    
    Expression negate() {
        Expression n = dup() ;
        for(int ci=0 ; ci < n.clauses.length;ci++)
        {
            n.clauses[ci] = n.clauses[ci].negate() ;
        }
        for(int li=0;li<n.litUsage.length;li++) {
            n.litUsage[li] =n.litUsage[li].negate() ;
        }
       return n ;
    }
    
    public String toString() {
        StringBuilder sb = new StringBuilder() ;
        sb.append( "Expression ").append(nLiveClauses).append(" live clauses and ").append(nUnBound).append(" free literals\n") ;
        for(int ci=0;ci<clauses.length;ci++) {
            Clause c = clauses[ci] ;
            if( c.terms != null) {
            sb.append("Clause[").append(ci).append("] (");
                for(int li=0;li<c.terms.length;li++)
                   sb.append(  c.terms[li]).append(" ");
                sb.append(") \n");
            }
            
        }
        sb.append("\nLiteralUsage\n");
        for(int li=0;li<litUsage.length;li++) {
            LiteralUsage lu = litUsage[li] ;
            sb.append("L_").append(li+1).append(": ");
            if(lu == null) sb.append(" <nousage>\n") ;
            else {
                sb.append(lu.nTotal - lu.nComplemented) . append("/") . append(lu.nComplemented).append(" ");
                sb.append("[ " );
                for(int k=0;k<lu.nTotal;k++)
                    sb.append(lu.inClauses[k]) . append(" ");
                
                sb.append("]\n");
            }
        }
        
        return sb.toString();
    }
    
    // the working assignment
    public final static byte BINDTRUE=0x1 , BINDFALSE=0x2 , UNBOUND=0  , CONSTRAINED=0x3 ;
    public final byte[] asg ;  // for each literal maintain its bound status
    public int nUnBound ;      // the number of unbound (free) literals remaining
    private final static boolean debugAssignment=false ;
    private ArrayList<iPair> constraints ;
    
    final LiteralUsage[] litUsage ;

    public int constrain(iPair ip) {
        int c = ip.i ;
        int b = ip.j ;
        int ci = c ;

        if( ci < 0) ci = -c;
        if(debugAssignment) System.out.println("Constrain(" + c +"," + b + ")");
        if( asg[ci-1] != UNBOUND ) {
            if(debugAssignment)Message.message("Literal[" + c + "] it is not unbound: " + asg[ci-1]);
            if(asg[ci-1] == CONSTRAINED) {
                ValHelper vh = new ValHelper();
                resolve(c,vh);
                if(debugAssignment)System.out.println("Resolved to: " + vh);
                if(vh.value == 0) {
                    // c is constrained by b
                    // but c is lready constrained by x
                    // so x and b have to be constrained by each other
                    int nc , nb ;
                    if( (b<0?(-b):b) <
                        (vh.asLiteral<0?(-vh.asLiteral):vh.asLiteral) ){
                        if( vh.asLiteral < 0) { nc = -vh.asLiteral ; nb = -b ; }
                        else { nc = vh.asLiteral ; nb = b ; }
                    }
                    else {
                        if( b < 0) { nc = -b ; nb = -vh.asLiteral ; }
                        else { nc = b ; nb = vh.asLiteral ; }
                    }
                    if(debugAssignment)System.out.println("Replacing constrain(" + c + "," + b + ") by constrain(" + nc + "," + nb + ")" );
                    ip.i = nc ;
                    ip.j = nb ;
                    return constrain(ip) ;
                }
                else return 0 ;
            }
            else return 0 ;
        }
                int bi = (b < 0)?(-b):b;
        if( asg[bi-1] != UNBOUND) {
            if(debugAssignment)Message.message("Attempted constrain(" + c + "," + b + ") - but B is not unbound: " + asg[bi-1]);
            if(asg[bi-1] == CONSTRAINED) {
                ValHelper vh = new ValHelper();
                resolve(b,vh);
                if(debugAssignment)System.out.println("Resolved to: " + vh + _toString());
                if( vh.value == 0) { ip.j = vh.asLiteral ; return constrain(ip) ; }
            }
            return 0 ;
        }
        if( b == c) return 0 ; // constraint already implied
        if( b == -c) return -1 ; // contradictory constraints
        
        asg[ci-1] = CONSTRAINED ;

        if(constraints == null) constraints = new ArrayList<>() ;
        if(c == b)
            Message.traceMessage(1,"Bad constraint: " + c + "," + b);
        constraints.add(new iPair(c,b)) ;
        nUnBound-- ;
        if(debugAssignment)
            System.out.println("Constrained literal " + (ci) + " => " + nUnBound + " unbound" );
        return 1 ;
    }
    
    private String getConstraint(int ci) {
        int cl = ci+1 ;
        if(constraints != null) 
            for( iPair cp : constraints) {
                if(cp.i == cl) return "@L" + cp.j ;
                if(cp.i == -cl) return "@L" + -cp.j ;
            }
        return "C?" ;
    } 
    
    void resolve( int lit , ValHelper tv) {
        int li = (lit<0?(-lit):lit)-1 ;

        switch(asg[li]) {
            case UNBOUND:
                tv.value = 0 ;
                tv.asLiteral = (short)lit ;
                break ;
            case BINDTRUE:
                if( lit > 0) tv.value = 1 ;
                else tv.value = -1 ;
                break ;
            case BINDFALSE:
                if( lit < 0) tv.value = 1;
                else tv.value = -1 ;
                break ;
            case CONSTRAINED:
                if(debugAssignment )System.out.print("Resolve( " + lit + ") is cosntrained ") ;
                for(iPair ip : constraints ) {
                    if( ip.i == lit) {
                        if(debugAssignment)System.out.println("by " + ip.j);
                        resolve( ip.j , tv);
                        return ;
                    }
                    else if(ip.i == -lit) {
                        if(debugAssignment)System.out.println("by " + -ip.j);
                        resolve( -ip.j , tv) ;
                        return ;
                    }
                }
                Message.traceMessage(1,"Could not resolve: " + lit);
                break ;
        }
    }
        
    public int getBinding(int p)  {
    int pi = ((p<0) ? -p : p) -1 ;

    int pval = asg[pi] ;
    if( pval == CONSTRAINED) {
        int by = 0 ;
        for(int ci=constraints.size()-1;ci >= 0; ci--) {
            iPair ip = constraints.get(ci);
            if( ip.i == p) { by = ip.j ; break ; }
            else if(ip.i == -p) { by = -ip.j ; break ; }
        }

      //  assert by != 0 ;
        // if( by == 0)Message.traceMessage(1,"Cannot find constrained");
        
        boolean flip = false ;
        if( by > 0) {
            pval = asg[ by -1] ;
            if( p < 0) flip=true ;
        }
        else {
            by = -by ;
            pval = asg[ by - 1 ];
            if( p > 0) flip = true ;

            
        }
        if(flip) { 
            if(pval == BINDTRUE) pval = BINDFALSE ;
            else if(pval == BINDFALSE) pval = BINDTRUE ;
        }
    }
    return pval ;
    
    }
    
    public boolean[] toBoolean() {
        boolean[] x = new boolean[asg.length] ;
        for(int i=0;i<x.length;i++)
            switch(asg[i]) {
                case BINDTRUE:
                    x[i] = true ;
                    break ;
                case BINDFALSE:
                    x[i] = false ;
                    break ;
            }
        return x ;
    }
    

private LiteralSet toSet ; // TreeSet<Literal> toSet ;
    public  boolean set( LiteralSet /*TreeSet<Literal>*/ hp , PairsMap pMap) throws SatResult {

        toSet = hp ;
        if (debugSet ) {
            System.out.println("Set called with set of " + hp.size() + " nLiveClauses=" + nLiveClauses + " nUnBound=" + nUnBound);
            consistencyCheck();
        //    System.out.println("Start set pt.1:\n" + toString());
        }
        
        while( true ) {

            
    while( ! hp.isEmpty()) {
        boolean possSU = false ;
        if(debugSet) {
            System.out.println("Top of loop in set with hp:");
            /*
                                Iterator<Literal> hit = hp.iterator() ;
                    while( hit.hasNext()) 
                        System.out.println("....." + hit.next().lv) ;
                    */
                    consistencyCheck() ;
        }
     //   Literal ihp = hp.first() ;
     //   hp.remove(ihp);
    //    int p = ihp.lv ;
        
        int p = hp.first();
        if(debugSet) System.out.println("Set " + p );
        
    int pi = (p>0 ? (p) : (-p) ) - 1;
    int av = asg[pi] ;
    if( av != 0)         {
        if(av == CONSTRAINED) {
                    if(debugSet ) System.out.println("Trying to set(" + p + ") ASG: " + _toString()) ;
            ValHelper vh = new ValHelper() ;
            resolve(p,vh);
            if(vh.value != 0) // so is T or F so we can proceed on that basis ???
            {   if( vh.value == -1) av = BINDFALSE ;
                else av = BINDFALSE ;
                if( p < 0 && av == BINDFALSE || p > 0 && av == BINDTRUE) continue ; // no op
                hp.clear();
                return false ; // is USAT
            }
            else
            {
                int np = vh.asLiteral ;
                if(debugSet) System.out.println("Replacing set(" + p + ") with set(" + np + ")" ) ;
                av = 0 ;
                p = np ;
                pi = (p>0 ? (p) : (-p) ) - 1;
            }
        }
        else {
            if( p < 0 && av == BINDFALSE || p > 0 && av == BINDTRUE) continue ; // no op
            hp.clear();
            return false ; // is USAT
        }
    }

    if(p < 0) asg[pi] = BINDFALSE ;
    else asg[pi] = BINDTRUE ;
    short literal = (short)(pi+1) ;

    LiteralUsage lu = litUsage[pi] ; 
    
    if(debugSet) System.out.println("setting litteral " + literal + " " + lu);
    int nRefs = lu.nTotal ;

    for( int ci=0;ci<nRefs;ci++) {
        int containingClause = lu.inClauses[ci] ;
        if(containingClause < 0) containingClause = - containingClause ;
        containingClause-- ; // stored to indicate sens of literal usage 
        Clause clause = clauses[ containingClause ] ;
        
        int inPos = -1 ;
        boolean killed = false ;
        if(debugSet) System.out.println("set(" + p + ") in clause[" + containingClause + "] " + clause) ;
        if(clause.terms == null)
            System.out.println("[" + pi + "] lu=" + lu + "\nnull terms literal=" + literal + " clauseIndex=" + ci + " containingClause=" + containingClause + "\n" + toString() );
        for( int cj=0;cj<clause.terms.length;cj++) {
            short trm = clause.terms[cj];
            if( trm == p) {  // so 3 and 3 - OR 3 and -3 so the same
                inPos = cj;
                break ;
            }
            else if( trm == -p) {
                killed = true ;
                inPos = cj;
                break ;    
            }
        }

        if(debugSet) System.out.println("\tinPos=" + inPos + " killed=" + killed) ;
        if( killed) {
            if(debugSet) System.out.println("Killing clause[" + containingClause + "] : " + clause);
            for(int cj=0;cj<clause.terms.length;cj++)
            { 
                    short trm = clause.terms[cj];
                    short ti = (short) ((trm<0?-trm:trm) - 1) ;
                    LiteralUsage lux = litUsage[ ti] ; 
                    if(debugSet) System.out.print("Drop " + trm + " in clause[" + containingClause + "] from l_" + (ti+1) + " " + lux );
                    litUsage[ti] = lux.dropFromClause( containingClause , trm < 0) ;
                    if(debugSet) System.out.println("=> " + litUsage[ti] );
                    if(ti != pi) {
                        lux = litUsage[ti] ; // may have changed
                    if(debugSet) System.out.println("Literal_" + (ti+1) + " " + lu);
                    if(lux.nTotal > 0 && lux.nComplemented == 0 ) possSU=true ;
                    else if( lux.nTotal > 0 && lux.nTotal == lux.nComplemented) possSU = true ;
                    }
                }
            
            if(debugSet) killClause( "SET(" + p + ") " , containingClause);             
            else killClause(containingClause);
            
            if(nLiveClauses == 0) {
                if(debugSet) System.out.println("No Live Clauses: " + toString());
                throw new SatResult(this) ;
            }
        } else {
            int ncl = clause.terms.length - 1 ;
            if(ncl == 0) {
                if(debugSet ) System.out.println("TRUE Clause[" + containingClause + "]" + clause + " " + _toString() );
                // dont kill it - it is TRUE so USAT
                return false ;
            }
            if(debugSet) System.out.print(" mod Clause[" + containingClause + "]" + clause  + " on SET(" + p + ")" );
            
            short[] nta = new short[ncl];
            int tw = 0 ;
            for(int cj=0;cj<clause.terms.length;cj++)
                if(cj != inPos) nta[tw++] = clause.terms[cj] ;
            clause =clauses[containingClause] = new Clause(nta) ; 

            if(debugSet) System.out.println(" => " + clause );
            if(ncl == 2) { newPairs++; }
            else if(ncl == 1) {
                hp.add( (short) - nta[0]) ; // new Literal( - nta[0] )) ; // single literal clause .. kill it
                if(debugSet) {
                    System.out.println("... add new literal to set: " + -nta[0] + " hp.first=" );
                }
            }
        }
       
        
    }


    litUsage[pi] = LiteralUsage.noUsage ;
    nUnBound--;
    if(possSU) {
        for(int li=0;li<litUsage.length;li++) {
            LiteralUsage lux = litUsage[li] ;
            if(lux.nTotal > 0 && lux.nComplemented == 0 ) {
               // System.out.println("Adding SU " + (-(li+1)) + " from " + lux);
                hp.add( (short)  - (li+1) ); // new Literal( - (li+1) )) ;
            } // SU all +
            else if( lux.nTotal > 0 && lux.nTotal == lux.nComplemented) {
                hp.add( (short) (  li+1 ) ) ;
            } 
        }
    }
    }
    // so we have fallen through the sets

   if(debugSet)  System.out.println("Done set pt.1: ") ;
   
    
    if(  newPairs < log2(nUnBound) ) {
        if(debugSet ) System.out.println( "done - not pairs npc=" + newPairs + " with nLiveClauses=" + nLiveClauses + " nUnBound=" + nUnBound); 
        if(nLiveClauses == 0) throw new SatResult(this);
        if(newPairs > 0) newPairs++ ;  // so it will eventually trigger checkForPairs
        return true ;
    } // not worth checking
    
    newPairs = 0 ;
    checkForPairs( pMap) ;

            if( ! pMap.contras.isEmpty()  && useCONSTRAINTS ) {
                    while(! pMap.contras.isEmpty() ) {
                    iPair cp = pMap.contras.remove(0);
                    if(debugCONSTRAINTS) System.out.println("Have constraint:" + cp );
                    if( constrain( cp.i , cp.j) == -1) // contradictory constraint SAT 
                       return false ;
                    }
            }

            if (hp.isEmpty()) {
                if(debugSet ) System.out.println( "done with nLiveClauses=" + nLiveClauses + " nUnBound=" + nUnBound); 
                        if(nLiveClauses == 0) throw new SatResult(this);
        return true ;
            }
            if (debugFVP) {
                System.out.println("Pairs found additional " + hp.size() + " sets");
            }
        } // end of endless loop
   
    }
   
   
    private final int log2(int n) {
        int p = 0;
        while( n > 0) {
            p++;
            n = n >> 1 ;
        }
        return p>0?(p-1):0 ;
    }

    private int newPairs =0 ; // to force an initial check
     
    private void killClause(int ci) {
       

        if(debugSet || debugCONSTRAINTS ) {
            int cix=ci+1 ; // how they are stres in inClause
            for(short t : clauses[ci].terms) {
            short literal = (short)(t <0 ? -t : t) ;
            LiteralUsage lu = litUsage[literal-1] ;
            for( int r=0;r<lu.nTotal;r++) {
                if( lu.inClauses[r] == cix || lu.inClauses[r] == -cix)
                    Message.traceMessage(1,"Clause being killed is still referenced literal  " + literal + ": "  + lu);
            }
        }
    }
            clauses[ci] = Clause.emptyClause ; //clause.terms = null ;
            nLiveClauses-- ;
    }
        
    private void killClause(String msg , int ci) {
        System.out.println("Killing clause[" + ci +"] " + msg);
        killClause(ci);
    }
    
    
    private final static boolean debugSet = false ;
    
    public String _toString() {
        String res = "ASG=" + nUnBound + " " ;
        for(int i=0;i<asg.length;i++)
            switch(asg[i]) {
                case BINDTRUE : res += "&" + (i+1) +  "=T" ; break ;
                case BINDFALSE: res += "&" + (i+1) +  "=F"; break ;
                case CONSTRAINED: res += "&" + (i+1) +  "=" + getConstraint(i); break ;
            }
        return res;
    }
    
    public String asgToString() {
        char[] res = new char[asg.length] ;
        for(int i=0;i<asg.length;i++)
            switch(asg[i]) {
                case BINDTRUE: res[i] = 'T' ; break ;
                case BINDFALSE: res[i] = 'F' ; break ;
                case UNBOUND: res[i] = '-' ; break ;
                
                default: res[i] = '*' ;
            }
        return "ASG: " + new String(res);
    }

   public String compatAbility(boolean[] cert) {
       String ubl = "" ;
       if(cert.length != asg.length) return "Inconsistent lengths " + cert.length + " vs " + asg.length;
       for(int i=0;i<asg.length;i++) 
           switch( asg[i]) {
               case BINDTRUE:
                   if(cert[i] != true) return "mismatch on literal " + (i+1) + " cert is F asg is T" ;
                   break;
               case BINDFALSE:
                   if(cert[i] != false) return "mismatch on literal " + (i+1) + " cert is T asg is F" ;
                   break;
               case UNBOUND:
                   ubl = ubl + " " + (i+1) ;
               
           }
       return "asg and cert are compatible. unbound=" + ubl ;    
   }
   public boolean isCompat(boolean[] cert) {
       if(cert.length != asg.length) return false ;
       for(int i=0;i<asg.length;i++) 
           switch( asg[i]) {
               case BINDTRUE:
                   if(cert[i] != true) return false ;
                   break;
               case BINDFALSE:
                   if(cert[i] != false) return false ;
                   break;
           }
       return true ;    
   }
   private void addLitUsage( int lit , boolean complemented , int clauseIndex) {
       if(litUsage[lit-1] == null) litUsage[lit-1] = new LiteralUsage() ;
       litUsage[lit-1].add(clauseIndex,complemented);
   }
   SatAssignment getAssignment() {
       SatAssignment x = new SatAssignment(asg.length);
       for(int l=0;l<asg.length;l++) {
           if( asg[l] == BINDTRUE) x.setTrue(l);
           else if( asg[l] == BINDFALSE) x.setFalse(l);
           else if( asg[l] == CONSTRAINED) {
                ValHelper vh = new ValHelper();
                resolve(l+1,vh);
                if(debugAssignment) System.out.println("getAssignment resolved(" + (l+1) + ") => " + vh);
                if( vh.value == 1) x.setTrue(l);
                else if( vh.value == -1) x.setFalse(l);
                else x.set(l,vh.asLiteral);
           }
       }
       return x ;
   }
  

       public int bestSplitOn() {
       int lx = -1 , mx = 0 ; 
 
       for(int l=litUsage.length-1;l>=0;l--) {
           LiteralUsage lu = litUsage[l] ;
           if(lu.nTotal > 0) {
               int ln = lu.nComplemented ;
               int lp = lu.nTotal - ln ;
               int c2n , c2c , c3n , c3c , nlong;
               nlong=c2n=c2c=c3n=c3c=0;
               
               for(int m=lu.nTotal-1;m>=0;m--) {
                   int isComp = -4 ; // boolean isComp = false ;
                   int cix = lu.inClauses[m] ;
                   if(cix<0) {
                       isComp = -3;
                       cix = -cix;
                   }

                   
                   switch( 2*clauses[cix-1].terms.length+isComp) {
                       case 0:
                           c2n++;
                           break;
                       case 1:
                           c2c++;
                           break;
                       case 2:
                           c3n++;
                           break ;
                       case 3:
                           c3c++;
                           break ;
                       default: nlong++ ;
                   }
               }
               
              int _coverage    =  (c2n<<3) + c3n ;
              int _altCoverage  =  (c2c<<3) + c3c ;

              int avgC = _coverage+_altCoverage + (_coverage*_altCoverage) + nlong ;
              
              if( lx < 0 ||  avgC > mx ) {
                  coverage = _coverage ;
                  altCoverage = _altCoverage ;
                  lx = l ;
                  mx=avgC ;
              }

               
           }
       }

       lx = lx+1 ; 
       if( coverage < altCoverage ) {
           lx = -lx ;
           mx = coverage ;
           coverage = altCoverage ;
           altCoverage = mx ;
       }
       double td = 100.0*(nUnBound+nLiveClauses) ;
       coverage    = (int)(td/coverage) ;
       altCoverage = (int)(td/altCoverage);
       return lx;
   }
         

   private void cnf_eval( byte[] xa, Expression xo) {
       StringBuilder sb = new StringBuilder() ;
       boolean failedClause = false ;
       for(int i=0;i<clauses.length;i++)
       {
           Clause clause = clauses[i] ;
           boolean sawTrue = false ;
           sb.append("Clause[" ).append(i+1).append("]").append(clause.toString()).append(" w asg ");
           if(clause.terms != null ) {
               for(int t=0;t<clause.terms.length;t++) {
                   short ltrm = clause.terms[t];
                   short trm = (short)(ltrm<0?-ltrm:ltrm) ;
                   int bv = xa[trm-1] ;
                   switch(bv) {
                       case UNBOUND:
                           sb.append("?");
                           break ;
                        case BINDTRUE:
                            if(! (ltrm < 0) ) sawTrue = true ;
                           sb.append(ltrm<0?"F":"T");
                           break ;
                        case BINDFALSE:
                            
                           if( ltrm<0) sawTrue = true ;
                           sb.append(ltrm<0?"T":"F");
                           break ;
                        case CONSTRAINED:
                           sb.append(ltrm<0?"!C":"C");
                           break ;       
                   }
               }
           }
           if(! sawTrue) {
               sb.append(" FAILED CLAUSE ");
               sb.append( xo.clauses[i].toString()) ;
               failedClause = true ;
           }
           else sb.append(" " ).append( xo.clauses[i].toString()) ;
           
           sb.append("\n");
       }
       
       if(! failedClause) {
           System.out.println("cnf_eval did not find a failedClause:\n" + sb.toString());
           System.exit(-1);
       }
   }
   
      boolean cnf_eval( SatAssignment sa ) {  // evaluate this expression given the passed in assignment
       boolean failedClause = false ;
       short[] nakedLiterals = new short[16];
       int nnaked  =0;
       for(int i=0;i<clauses.length;i++)
       {
           Clause clause = clauses[i] ;
           boolean sawTrue = false ;
           if(clause.terms != null ) {
               nnaked = 0 ;
               for(int t=0;t<clause.terms.length;t++) {
                   short ltrm = clause.terms[t];
                   short trm = (short)(ltrm<0?-ltrm:ltrm) ;
                   int bv = sa.lasg[trm-1] ;
                   switch(bv) {
                        case SatAssignment.TRUE:
                            if(! (ltrm < 0)) sawTrue = true ;
                           break ;
                        case SatAssignment.FALSE:
                            
                           if( ltrm<0 ) sawTrue = true ;
                           break ;
                        case SatAssignment.BAD: // means not assigned so naked
                            int bnaked = ltrm ; 
                            for(int k=0;k<nnaked;k++)
                                if( nakedLiterals[k] == bnaked)
                                    sawTrue = true ;
                            nakedLiterals[nnaked++] = (short) bnaked ;
                            break ;
                        default:
                            ValHelper vh = new ValHelper() ;
                            sa.resolve( ltrm , vh) ;
                            if(debugSet) System.out.println("Term : " + ltrm + " resolves to " + vh);
                            switch(vh.value) {
                                case -1: // false
                                    // no op n this context
                                    break;
                                case 0: // literal
                            int naked = vh.asLiteral  ;
                            for(int k=0;k<nnaked;k++)
                                if( nakedLiterals[k] == - naked)
                                    sawTrue = true ;
                            nakedLiterals[nnaked++] = (short) naked ;
                            break;
                                case 1:
                                    sawTrue = true ;
                                    break ;
                                    
                            }
                            
                   }
                   if(sawTrue) break ;
               }
           }
           if(! sawTrue) {
               if( nnaked == 0) {
                   Message.message("Clause[" + i + "] =" + clause + " fails " );
                   failedClause = true ;
               }
               else 
                   Message.message("Clause[ " + i + "]=" + clause + " Indet[" + nnaked + "] naked literals " + Arrays.toString(nakedLiterals));
               
           }
           
       }

       if(! failedClause) return true ;
       return false ;
   }
    
      // coverage and comparator stuff for prioritization of subProblems
    int coverage , altCoverage  ;

    public int compareTo(Expression ox) {
        return coverage - ox.coverage ;
    }
    
// pairs stuff - references here - actual data structures are per solver thread.
private byte[] map ;
private int cycle ;  // used to avoid having to clear the map on every use which is costly
private ArrayList<iPair> contras ;
private ArrayList<Integer> addedClauses ;
private final static int PP = 0x8 , PN = 0x4 , NP=0x2 , NN=0x1 ;
private static final boolean debugFVP=false ;

private final static boolean useCONSTRAINTS=true ;
private final static boolean debugCONSTRAINTS =false ;


private void checkForPairs(PairsMap pMap) throws SatResult {
    map = pMap.map ;
    cycle = pMap.next() ; // clso clears the contras array
    contras = pMap.contras ;
    contras.clear() ;
    addedClauses = pMap.tempClauses ;

    for(int ci=clauses.length-1;ci>=0;ci--) {
        Clause clause = clauses[ci];
        if( clause.terms != null && clause.terms.length == 2)
                    checkMap( ci , clause);
            }

}


private void checkMap(int cn , Clause cl  ) throws SatResult {
                short[] terms = cl.terms ;

                int wp ; // order is 0x8=++ , 0x4=+- , 0x2=-+ , 0x1=--
                int sl0 = terms[0] ;
                int sl1 = terms[1];
                
                int l0 = sl0<0?-sl0:sl0  ;  // literals are 1.. indices are 0...
                int l1 = sl1<0?-sl1:sl1  ;
                if(debugFVP) Message.message("debugFVP checkMap.new(" + terms[0] + "," + terms[1] + ")") ;
                int i0 = l0 -1;
                int i1 = l1 - 1 ;
                if( ! (sl0 < 0) ) { 
                    if( ! (sl1 < 0))  wp = 0x8; 
                    else  wp = 0x4 ;
                
                }
                else {
                    if( ! (sl1<1) ) wp = 0x2; 
                    else  wp = 0x1 ;
                }
                // i = (n * r) + c – ((r * (r+1)) / 2)   if using the triangular property
                // but by storing 2 per byte we dont have a square to begin with ....
                int ix = i0*asg.length+i1 ;

                byte m = map[ix];
                if(debugFVP) Message.message("\t map had " + m + " now add in " + wp) ;
                if( (m == 0) || (((m>>4)&0xF) != cycle) ) {  // either nothing or old data, this is the first pair
                    map[ix] = (byte)((cycle<<4)|wp);
                    return ;
                }
                
                byte    nm = (byte)( m | wp) ;
                if( nm == m) {
                    if(debugFVP) System.out.println("Have dup(" + l0 + "," + l1 + ") in " + cl )  ;
                   
                    
                   litUsage[i0] = litUsage[i0].dropFromClause(cn, sl0<0);
                   litUsage[i1] = litUsage[i1].dropFromClause(cn, sl1<0);
                    if(debugSet) killClause( "Have dup: "  , cn);
                    else killClause(cn);
                    return ;
                  
                } // was seen before - we can have dups COULD REMOVE THE DUP HERE
                
                map[ix] = nm ;   // save that we saw l0,l1 pair
                if((nm&0xF) == 0xF) { 
                    
                    throw new SatResult(); /* we have all 4 minterms IS USAT */
                }
                
                int    pw = (0xF & m) ;
                
                    switch(wp) {
                        case PP:
                            if( (pw & PN) != 0) toSet.add( (short) - sl0 ) ; 
                            else if( (pw & NP) != 0) toSet.add( (short) - sl1) ; 
                            else if( (pw&NN) != 0) contras.add( new iPair(sl0,- sl1)) ; // make different
                        break ;
                        case PN:
                            if( (pw & PP) != 0) toSet.add( (short) - sl0 ); 
                            else if( (pw & NN) != 0) toSet.add( (short) - sl1); 
                            else if( (pw&NP) != 0) contras.add( new iPair(sl0 ,- sl1)) ; // make the same
                        break ;
                        case NP:
                            if( (pw & PP) != 0) toSet.add((short) - sl1 ); 
                            else if( (pw & NN) != 0) toSet.add((short) - sl0);
                            else if( (pw&PN) != 0) contras.add( new iPair(-sl0,sl1)) ; // make the same
                        break ;
                        case NN:
                            if( (pw & PN) != 0) toSet.add((short) - sl1); 
                            else if( (pw & NP) != 0) toSet.add((short)- sl0 ); 
                            else if( (pw&PP) != 0) contras.add( new iPair(-sl0,sl1)) ; // make different
                        break ;
                        default:
                        Message.traceMessage(1,"Impossible wp=" + wp);
                            
                    }

}
private int constrain(int by , int cons ) {
    if(debugCONSTRAINTS) System.out.println("constrain(" + by + "," + cons + ") nLiveClauses=" + nLiveClauses);
    iPair ip = new iPair(cons,by);
    int cv = constrain(ip) ;
    if( cv != 1) return cv ;
    if( ip.i != cons || ip.j != by) {
        cons = ip.i;
        by = ip.j ;

    }    
    int consi , byi ;
    if(cons < 0) {
   if(debugCONSTRAINTS)     
       System.out.println("Switching cons/by: " );
        cons = -cons ;
        by = -by ;
    }
 if(debugCONSTRAINTS) {
     System.out.println("Literal " + cons + " is equal to literal " + by + " constraint" );
 }
    consi = cons-1;
    int bylit = by ;
    if(by < 0) {
        bylit = -by ;
        byi = (-by) - 1;
    }
    else {
        byi = by - 1 ;
    }
    
    
    LiteralUsage lcons = litUsage[consi];
    addedClauses.clear() ;

    int cAdded = 0 ;
    // for the clauses containing cons
    for(int j=0;j<lcons.nTotal;j++) {
        int ci = lcons.inClauses[j] ;
        if(ci < 0) ci = -ci ;
        ci-- ;
        
        Clause clause = clauses[ci];

        int R=0 ; // set +1 if we see by and -1 if -by
        int inPos = -1;
        for(int k=0;k<clause.terms.length;k++) {
            short sterm = clause.terms[k];
            short term_l = (short)( sterm<0?-sterm:sterm) ;
            if( cons == term_l)  inPos = k ; 
            else if( bylit == term_l) {
                R = sterm ; // term.isComplement ? -term.literal : term.literal ;
            }
        }
        if(R == 0) {
         //   assert inPos >= 0 ;
            int bval = clause.terms[inPos] < 0 ? (- by ) : ( by ) ;
            short[] nterms = new short [clause.terms.length];
            for(int n=0;n<nterms.length;n++) {
                if(n != inPos) nterms[n] = clause.terms[n];
                else nterms[n] = (short) bval ;
            }
            if(bval < 0)  cAdded++ ;
            boolean done = false;
            bval = (by<0 ? (-by) : by) ;
            short tt ;
            
            while(! done) {
            if( inPos > 0 && bval < (nterms[inPos-1]<0?-nterms[inPos-1]:nterms[inPos-1]) ) 
            { tt = nterms[inPos-1]; nterms[inPos-1] = nterms[inPos]; nterms[inPos] = tt ; inPos--; }
            else if( inPos < nterms.length-1 && bval > (nterms[inPos+1]<0?-nterms[inPos+1]:nterms[inPos+1]) ) 
            { tt = nterms[inPos+1]; nterms[inPos+1] = nterms[inPos]; nterms[inPos] = tt ; inPos++; }
            else done = true ;
        }
            if(debugCONSTRAINTS)System.out.print(" CONS(" + cons + "," + by + ") Replace Clause[" + ci + "]="  + clauses[ci] + " with ");
            clauses[ci] = new Clause(nterms);
            if(debugCONSTRAINTS)System.out.println(clauses[ci]);
            addedClauses.add(new Integer(lcons.inClauses[j]));
            
        }
        else if( R == by &&  (clause.terms[inPos]<0) || R == (-by) && ! (clause.terms[inPos] < 0 ) ) {
           if(debugCONSTRAINTS) System.out.println("Killing Claus[" + ci + "] " + clause);

            for(int l=0;l<clause.terms.length;l++) {
                int m= clause.terms[l]<0?-clause.terms[l]:clause.terms[l];
                { 
                    LiteralUsage lx = litUsage[m-1];
                    if(debugCONSTRAINTS) System.out.println("\tremoving from L_" + m + " " + lx );
                    litUsage[m-1] = lx.dropFromClause(ci, clause.terms[l] < 0) ;
                if(debugCONSTRAINTS)    System.out.println(" => " + litUsage[m-1] );
                }
            }
            if(debugSet) killClause("Constrain(" + cons + "," + by + ") "  , ci);
            else killClause(ci);
        }
        else {
         if(debugCONSTRAINTS)   Message.message("Constrain(" + cons + " by " + by + ") Already present: " + clause );
            if( clause.terms.length > 0 ) {
            short[] nterms = new short[clause.terms.length-1];
            int w = 0 ;
            for(int z=0;z<clause.terms.length;z++)
                if(z != inPos)
                    nterms[w++] = clause.terms[z];
            clauses[ci] = new Clause(nterms) ;
            if( nterms.length == 1)
                toSet.add( (short) (- nterms[0])); // creating a singleton
            }
            else {
                clauses[ci] = Clause.emptyClause ;
                nLiveClauses-- ;
            }
            if(debugCONSTRAINTS)   Message.message(" => " + clause);
        }
    }
         litUsage[cons-1] = LiteralUsage.noUsage ;
         
         LiteralUsage lby = litUsage[byi] ;
         if(debugCONSTRAINTS)
             System.out.print("Expanding L_" + by + " " + lby + " with " + addedClauses.size() + " clauses");
         lby = litUsage[byi] = lby.expand(cAdded,addedClauses);
         // Should check for SU literals here they do occur
         if( lby.nComplemented == 0) toSet.add( (short) - (byi+1) ); // new Literal(- (byi+1) )) ; // SU all non complemented
         else if( lby.nTotal == lby.nComplemented) toSet.add( (short)  (byi+1)); // new Literal( (byi+1))) ; // su all complemented
         if(debugCONSTRAINTS)
             System.out.println(" => " + litUsage[byi]);
         
    if (debugCONSTRAINTS) {
        System.out.println("Constrain done: ");

        for (int l = 0; l < asg.length; l++) {
            if (litUsage[l].nTotal == 0 && asg[l] == 0) {
                Message.message( "L_" + (l + 1) + " no usage not assigned ASGV=" + _toString() + ":\n" + toString() );
            }
        }
    }
    return cv ; // done
}

private void consistencyCheck() {
    for(int l=0;l<litUsage.length;l++) {
        LiteralUsage lu = litUsage[l];
        for( int m=0;m<lu.nTotal;m++) {
            int cix = lu.inClauses[m];
            if(cix < 0) cix = - cix ;
            cix -- ;
            if(clauses[cix].terms == null)
                Message.traceMessage(1,"Bad clause reference from L_" + (l+1) +": " + lu + " " + lu.inClauses[m] + " => clause[" + cix + "] " + _toString());
        }
    }
}
   
}


