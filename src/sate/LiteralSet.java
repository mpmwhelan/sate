/*
 * The MIT License
 *
 * Copyright 2017 Michael Whelan, E-insights Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package sate;

/**
 *
 * @author whelan
 */
public class LiteralSet {
    private final int nLits ;
    private final byte[] isMember ;
    private final static int INITCAPACITY=32;
    int size ;
    public LiteralSet(int nl) {
        nLits = nl ;
        isMember = new byte[nl];
        members = new short[INITCAPACITY];
        size = 0 ;
    }
    private short[] members ;
    public void clear() {
        size = 0 ;
        for(int i=isMember.length-1;i>=0;i--)
            isMember[i]=0;
    }
    public void add(short l) {
        int li ;
        if(l < 0) {
            li = (-l)-1;
            if( (isMember[li] & 0x2) != 0) return ;
            isMember[li] |= 0x2 ;
        }
        else {
            li = l-1;
            if( (isMember[li] & 0x1) != 0) return ;
            isMember[li] |= 0x1 ;
        }
        if( size >= members.length) {
            short[] nm = new short[members.length+INITCAPACITY];
            System.arraycopy(members,0,nm,0,members.length);
            members = nm ;
        }
        members[size++] = l;
        
    }
    public short first() {
        if(--size < 0) Message.traceMessage(1,"Empty set");
        short rv = members[size];
        // we could unset the isMember indicator -...
        return rv ;
    }
    public int size() { return size ; }
    public boolean isEmpty() { return size == 0; }
}
