/*
 * The MIT License
 *
 * Copyright 2017 Michael Whelan, E-insights Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package sate;
import java.io.* ;
import java.util.ArrayList ;
import java.nio.file.DirectoryStream ;
import java.nio.file.Path ;
import java.nio.file.Paths ;
import java.nio.file.Files ;
import java.text.DecimalFormat ;




/**
 *
 * @author whelan
 */
public class MT_SATe {

        private static String CNFdir = "./" ;  // path where the test case files can be found
        private final static String[] timeTests = { // some b uilt in time tests against SATLIB test cases
    
       "label=uf150    limit=0  pattern=uf150- threads=8" ,
       "label=uuf150   limit=0  pattern=uuf150- threads=8" ,
       "label=uf175    limit=0  pattern=uf175-*.cnf threads=8" ,
       "label=uuf175   limit=0  pattern=uuf175- threads=8"  ,
       "label=uf250    limit=20 pattern=uf250-*.cnf threads=8" ,
       "label=uuf250   limit=20 pattern=uuf250-*.cnf threads=8" ,
    } ;   
        
    
    public static void main(String[] args) throws Exception {
        MT_SATe ts ;
        if(args.length == 0) {
            for(String trun : timeTests) {
                String[] targs = trun.split("\\s+");
                ts = new MT_SATe(targs);
                ts.run();
            }   
        }
        else {
            ts = new MT_SATe(args);
            ts.run();
        }
        
        System.exit(0);
        
    }
    
    private int nthreads = 1;
    private int problemsLimit = 0 ;
    private String pattern = null ;
    private String label = null ;
    private int maxSecondsToRunEachSAT = 60*60 ; // one hour
    
    private ArrayList<String> fnames = new ArrayList<>() ;

    public MT_SATe(String[] args) {
        int nadded = 0 ;
        int maxThreads = Runtime.getRuntime().availableProcessors() ;  
        nthreads = maxThreads ;// default for MT
        for (String s : args) {
            int eqindex = s.indexOf("=");
            if (eqindex < 0 ) {
                if(problemsLimit <= 0 || (nadded < problemsLimit) ) 
                {
                nadded++ ;
                fnames.add(s);
            } 
        } else {
                String arg = s.substring(0, eqindex).toLowerCase();
                String val = s.substring(eqindex + 1);
                if (arg.equals(("threads"))) {
                    nthreads = Integer.parseInt(val);
                if( nthreads < 1 || nthreads > maxThreads) {
                       System.out.println("Threads outside acceptable rangs [1.." + maxThreads + "] at " + nthreads) ;
                       System.exit(-1);
                      }
                } else if (arg.equals("limit")) {
                    problemsLimit = Integer.parseInt(val);
                }  else if (arg.equals("directory")) {
                    CNFdir = val ;
                }  else if (arg.equals("prefix")  || arg.equals("pattern")) {  // for compat with old test cases
                    if(val.endsWith(".cnf")) pattern = val;
                    else pattern = val + "*.cnf" ;
                    pattern = val + "*";
                }   else if (arg.equals("label")) {
                    label = val ;
                } else if (arg.equals("check")) {
                    fnames.add(val + ".cnf") ;
                    if(! val.startsWith("/" )) val = CNFdir + "/" + val + ".sat" ;
                    
                    System.out.println("Certificate checking not yet supported");
                    System.exit(-1);
                    
                } else if(arg.equals("-cpu-lim") || arg.equals("cpu-lim")) {
                    
                if(val.contains(":")) maxSecondsToRunEachSAT = toSecs(val);
                else maxSecondsToRunEachSAT = Integer.parseInt(val);
                }
                else {
                    System.err.println("Unknown flag: " + s);
                }
            }
        }

        if ( pattern != null ) {
            try (DirectoryStream<Path> stream
                    = Files.newDirectoryStream(Paths.get(CNFdir), pattern)) {
                for (Path entry : stream) {
                     if (problemsLimit <= 0 || nadded < problemsLimit) {
                        fnames.add(entry.getFileName().toString());
                        nadded++;
                    }
                }
            } catch (Exception x) {
                System.err.println(x);
                System.exit(-1);
            }

        }
    }
        
        public void run() {
            ArrayList<String> errors = new ArrayList<>() ;

            if(fnames.size() <= 0) {
                System.out.println("Nothing to do.") ;
                return ;
            }
            

        for(int wc=0;wc<=0;wc++) {
            long tmsecs = 0 ;
            int ns  = 0 ;
            errors.clear();

        MT_Solver solver = new MT_Solver(nthreads,maxSecondsToRunEachSAT) ;
        for( String fn : fnames ) {
            try {
            if(label == null) System.out.print(fn + ": " );
            
         Expression xorig = NormalForm.read( new File( (fn.startsWith(File.separator) || fn.startsWith(".")) ? fn : CNFdir + File.separator + fn )) ;
        // System.out.println("Original : " + xorig);
        
        long sStart = System.currentTimeMillis() ;
         SatAssignment sat = solver.sat(xorig.negate()) ;
         long tDel = System.currentTimeMillis() - sStart ;
         ns++;
         tmsecs += tDel ;
        if(label==null) System.out.print( showSeconds(tDel) + " s " + fn + " SAT: " ) ;
         if(sat != null) {
                boolean res = xorig.cnf_eval(sat) ;
                     if(label==null) System.out.println( res + " on " + sat);
                     if(fn.startsWith("uf") && res == false) errors.add(fn);
                     else if( fn.startsWith("uuf") && res == true) errors.add(fn);
         }
         else {
              if(fn.startsWith("uf") ) errors.add(fn);
             if(label==null) System.out.println(" NOTSAT");
         }

         
        } catch(Exception x) {System.out.println("SATe.RUN caught " + x);
        x.printStackTrace(System.out);
        System.exit(-1);
        }
        
        } // end each file name
        solver.shutDown() ;
        
        System.out.println(label==null?"":(label+": ") + "In total " + ns + " files with " + errors.size() + " errors in " + showSeconds(tmsecs)  + " S => avg=" + showSeconds(tmsecs/ns)+" S" );
        if(errors.size() > 0) {
            System.out.println("Have " + errors.size() + " errors");
            for(String e : errors)
                System.out.println(e);
        }
        }
    }
   
        private int toSecs(String ts) {
            String[] pts = ts.split(":");
            int s = 0 ;
            for(int k=0;k<pts.length;k++)
                s = (s*60) + Integer.parseInt(pts[k]);
            return s ;
        }
        private final DecimalFormat dfmt = new DecimalFormat("0.###");
        
        private String showSeconds( long ms) {
            double ns = 0.001*(ms);
            return dfmt.format(ns);
        }
    
}
