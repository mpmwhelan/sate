/*
 * The MIT License
 *
 * Copyright 2017 Michael Whelan, E-insights Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package sate;
import java.util.ArrayList ;

/**
 *
 * @author whelan
 * 
 * Supports identifying Fixed Variable Pairs and Contrarian PAirs.
 * 
 */
public class PairsMap {
    final byte[] map ;
    final int NS ;
    int cycle ;
    final ArrayList<iPair> contras ;
    final ArrayList<Integer> tempClauses ;

    PairsMap(int ns) {
        map = new byte[ns*ns] ;
        NS = ns ;
        cycle = 0 ;
        contras = new ArrayList<>() ;
        tempClauses = new ArrayList<>();

    }
    int next() {
        contras.clear() ;
        cycle = (cycle+1)&0xF; // 4 bits .. 0 .. 15
        if(cycle == 0) {
            for(int k=0;k<NS;k++)
                for(int l=k+1;l<NS;l++) {
                int ix = k*NS+l;
                 if(map[ix] != 0) 
                    map[ix]=0;
            }

        }
        return cycle ;
    }
}
