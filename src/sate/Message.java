/*
 * The MIT License
 *
 * Copyright 2017 Michael Whelan, E-insights Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package sate;


/**
 *
 * @author whelan
 * 
 * Helper class for displaying information and possibly terminating.
 * For debugging purposes.
 */
public class Message {
    public static void message(String m)
    {
        System.out.println( m );
    }
    public static void exitMessage(String m) {
        message(m);
        System.exit(-1);
    }
    public static void traceMessage(String m) {
    traceMessage(0,m);
    }
 
    public static void traceMessage(int exitCode , String m) {
        message(m);
        try {
            throw new Exception(" traced to");
        }
        catch(Exception x) {
            x.printStackTrace(System.out);
        }
        if(exitCode != 0) System.exit(exitCode);
    }
}
