/*
 * The MIT License
 *
 * Copyright 2017 Michael Whelan, E-insights Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package sate;

/**
 *
 * @author whelan
 */
public class SatAssignment {
    static final short BAD=3 , TRUE=1 , FALSE=2 , MASK=0x3 ;
    short[] lasg ;
    public SatAssignment(int s) { lasg = new short[s];
    for(int l=0;l<lasg.length;l++) lasg[l] = BAD;
    }
    public void setTrue(int l) { lasg[l] = TRUE ; }
    public void setFalse(int l) { lasg[l] = FALSE ; }
    public void set(int l , int v ) { lasg[l] = (short) (v<<2) ; }
    public void clear() { for(int i=0;i<lasg.length;i++) if(lasg[i]!=BAD) lasg[i]=BAD; }
    
    public String toString() {
        StringBuilder sb = new StringBuilder() ;
        for(int l=0;l<lasg.length;l++) {
            int lv = lasg[l];
            switch(lv&MASK) {
                case 0:
                    sb.append("b");  // bound/constrained by something
                    break ;
                case TRUE:
                    sb.append("T");
                    break ;
                case FALSE:
                    sb.append("F");
                    break ;
                case BAD:
                    sb.append("u"); // unbound-naked
                    break ;
            }
        }
        return sb.toString() ;
    }
    
    public void resolve(int l , ValHelper vh) {
        int li = (l<0?(-l):l)-1;
        //System.out.println("sa.resolve(" + l +") sa[" + li + "]= " + lasg[li]);
        
        switch( lasg[li] & MASK) {
            case 0:
                resolve( lasg[li]>>2 , vh ) ;
                if(l < 0) vh.negate() ;
                break ;
                
            case TRUE:
                if( l < 0) vh.setFalse();
                else vh.setTrue() ;
                break ;
                
            case FALSE:
                if( l > 0) vh.setFalse();
                else vh.setTrue() ;
                break ;
            case BAD:  // unbound literal
                vh.setLiteral(l);
                break ;
            
        }
    }
}
