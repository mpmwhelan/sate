/*
 * The MIT License
 *
 * Copyright 2017 Michael Whelan, E-insights Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package sate;

/**
 *
 * @author whelan
 * 
 * The worker portion for an individual thread
 * 
 */
public class SolverWorker implements  Runnable {
    private final MT_Solver myMts ;
    private final int Id ;

    public SolverWorker(int i , MT_Solver mts) {
        myMts = mts ;
        Id = i ;
    }
    
    private PairsMap fvpmap ;
    private LiteralSet toSet ; // final TreeSet<Literal> toSet  ;
    private final static boolean debug = false ;

    public void run() {
        Literal lit = new Literal(0);
        boolean done = false;
        while (true) {
            SubProblem myProb = null;
            if (debug) {
                System.out.println("Worker_" + Id + " looking for workq");
            }
            try {
                myProb = myMts.wakeQ.take();
            } catch (InterruptedException ix) {
                System.out.println("Thread interrupted");
                done = true;
            }
            if (debug) {
                System.out.println("Have x to split on #Clauses=" + myProb.x.nLiveClauses + " splitOn: " + myProb.splitOn);
            }
            if(myProb.splitOn == 0) return ; // we are done

            while (myProb != null) {
                Expression myX = myProb.x;
                if (fvpmap == null) {  // first usage , allocate objects for checking pairs etc.
                    fvpmap = new PairsMap(myX.asg.length);
                    toSet = new LiteralSet(myX.asg.length);
                } else { // re-use , dont need to clear the fvpmap - the 'cycle' handles that as needed
                    toSet.clear();
                }
                
                toSet.add((short) myProb.splitOn);

                try {  // there are situations where it is easier to throw an exception represent a result 

                    if (myX.set(toSet, fvpmap)) {
                        myProb.splitOn = myX.bestSplitOn() ;
                        myProb.needToDup = true ;// new split both ways needed
                        myProb = myMts.spDone(myProb);
                        if(debug ) System.out.println("spdone gave back: " + myProb.x.nLiveClauses + " on " + myProb.splitOn );
                    } else {
                        if (debug ) {
                            System.out.println("sub prob is SAT");
                        }
                        myProb.x = null ;
                        myProb = myMts.spDone(myProb);
                    }
                } catch (SatResult sr) {
                    if (sr.satX != null) { // we have a solution ...
                        myMts.haveSat(myX);
                        myProb = null ; // so we go back to wakeQ.take
                    }
                    else { 
                        if(debug) System.out.println("Worker_" + Id + " int with USAT");
                        myProb.x = null ;
                        myProb = myMts.spDone(myProb);
                    }
                }
            } // mhile myProb != null
        } // while true
    }
               
}
