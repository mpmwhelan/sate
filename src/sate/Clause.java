/*
 * The MIT License
 *
 * Copyright 2017 Michael Whelan, E-insights Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package sate;

/**
 *
 * @author whelan
 * Represent a clause with the literals in the terms array - negation indicating complemented use - ala DICMACS
 */
public class Clause {
    public final static Clause emptyClause = new Clause() ;
    
    private Clause() { terms = null ; }
    final short[] terms ;
    public Clause( short[] lits ){
        terms = lits ;
    }

    public Clause(Clause c) {
        if(c.terms != null) {
            terms = new short[c.terms.length] ;
            System.arraycopy(c.terms,0,terms,0,c.terms.length);
        }
        else terms = null ;
    }
    public Clause negate() {
        short[] nterms = new short[terms.length];
        
        for(int i=terms.length-1;i>=0;i--)
            nterms[i] = (short) -terms[i];
        return new Clause( nterms);
    }
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" nTerms=");
        if (terms == null) {
            sb.append("<nilN>");
        } else {
            sb.append(terms.length).append(" [");

            for (int t = 0; t < terms.length; t++) {
                sb.append(terms[t]).append(" ");
            }
            sb.append("]");
        }
        return sb.toString();

    }
    
}
